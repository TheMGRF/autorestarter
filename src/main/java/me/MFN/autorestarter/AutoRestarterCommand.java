package me.MFN.autorestarter;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AutoRestarterCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!sender.hasPermission("autorestarter.admin")) {
            sender.sendMessage(ChatColor.RED + "You do not have access to this command!");
            return true;
        }

        Main.getInstance().reload();
        sender.sendMessage(ChatColor.GREEN + "Auto restarter config reloaded!");

        return true;
    }

}
