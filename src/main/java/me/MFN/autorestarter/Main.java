package me.MFN.autorestarter;

import java.text.SimpleDateFormat;
import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {

	private static Main instance;

	private int restartHour;
	private String prefix = "";
	private Map<Integer, String> minutes = new HashMap<Integer, String>();
	private Map<Integer, String> seconds = new HashMap<Integer, String>();
	
	public void onEnable() {
		instance = this;

		getCommand("autorestarter").setExecutor(new AutoRestarterCommand());

		reload();

		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				sdf.format(cal.getTime());

				if (cal.get(Calendar.HOUR_OF_DAY) == (restartHour - 1)) {
					final int minute = cal.get(Calendar.MINUTE);
					final int second = cal.get(Calendar.SECOND);

					if (cal.get(Calendar.MINUTE) >= 59) {
						if (seconds.containsKey(second)) {
							String msg = seconds.get(second);
							for (Player p : getServer().getOnlinePlayers()) {
								p.sendTitle(msg, "", 0, 60, 10);
							}
						}
					} else {
						if (minutes.containsKey(minute)) {
							if (cal.get(Calendar.SECOND) <= 1) {
								String msg = minutes.get(minute);
								for (Player p : getServer().getOnlinePlayers()) {
									p.sendTitle(msg, "", 0, 60, 10);
								}
							}
						}
					}
				} else if (cal.get(Calendar.HOUR_OF_DAY) == restartHour) {
					if (cal.get(Calendar.MINUTE) < 1) {
						Bukkit.broadcastMessage(prefix + "§d§lRESTARTING!");
						for (Player p : getServer().getOnlinePlayers()) {
							p.sendTitle("§d§lRESTARTING!", "", 0, 60, 10);
						}

						getServer().getScheduler().runTaskLater(getInstance(), new Runnable() {
							@Override
							public void run() {
								getServer().shutdown();
							}
						}, 1);
					}
				}
			}
		}, 0, 20);
	}

	public static Main getInstance() { return instance; }

	public void reload() {
		saveDefaultConfig();
		reloadConfig();

		minutes.clear();
		seconds.clear();

		restartHour = getConfig().getInt("restart-hour");
		prefix = getConfig().getString("prefix");
		
		ConfigurationSection m, s;
		if (!getConfig().isConfigurationSection("minutes")) {
			m = getConfig().createSection("minutes");
			s = getConfig().createSection("seconds");
		} else {
			m = getConfig().getConfigurationSection("minutes");
			s = getConfig().getConfigurationSection("seconds");
		}
		
		for (String minute : m.getKeys(false)) {
			try {
				minutes.put(Integer.parseInt(minute), ChatColor.translateAlternateColorCodes('&', m.getString(minute + "")));
			} catch (NumberFormatException e) {
				getLogger().severe("Failed to load auto restart messages '" + minute + "'!");
			}
		}

		for (String second : s.getKeys(false)) {
			try {
				seconds.put(Integer.parseInt(second), ChatColor.translateAlternateColorCodes('&', s.getString(second + "")));
			} catch (NumberFormatException e) {
				getLogger().severe("Failed to load auto restart messages '" + second + "'!");
			}
		}
	}
}
